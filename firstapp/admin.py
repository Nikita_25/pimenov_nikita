from django.contrib import admin
from .models import Dish
from .models import CustomUser

# Register your models here.
admin.site.register(Dish)
admin.site.register(CustomUser)
