import jwt
from django.conf import settings
from rest_framework import authentication, exceptions
from .models import CustomUser
from datetime import datetime

class JWTAuthentication(authentication.BaseAuthentication):

    authentication_header_prefix = 'Token'

    def authenticate(self, request):
        '''
        Функция authenticate будет вызываться фреймворком для всех views
        с аттрибутом permission_classes = [IsAuthenticated]
        потому что класс JWTAuthentication мы указали
        как средство аутентифиакции в settings.py
        '''
        request.user = None
        '''
        get_authorization_header вернет значение
        http заголовка "Authorization", в котороv клиент
        должен указать слово Token (т.н. префикс), а затем
        собственно токен, через пробел

        Выглядит этот заголовок вот так: "Authorization: Token твой_токен_здесь"

        split() разделит эту строку на слова и вернёт массив
        '''
        auth_header = authentication.get_authorization_header(request).split()
        auth_header_prefix = self.authentication_header_prefix.lower()

        if not auth_header:
            return None
        # в заголовке должно быть два слова
        if len(auth_header) != 2:
            return None

        prefix = auth_header[0].decode('utf-8')
        token = auth_header[1].decode('utf-8')

        if prefix.lower() != auth_header_prefix:
            return None

        return self._authenticate_credentials(request, token)

    def _authenticate_credentials(self, request, token):
        try:
            payload = jwt.decode(token, settings.SECRET_KEY)
        except:
            msg = 'Could not decode token'
            raise exceptions.AuthenticationFailed(msg)
        '''

        payload['exp'] содержит дату в строковом формате
        datatime.now() содержит дату в виде числа миллисекунд
        чтобы их сравнить, нужно привести к одному типу, но я забыл как

        может, .timestump()?

        ---
        if datetime.now() > payload['exp'] or datetime.now() == payload['exp']:
            msg = 'The token has expired'
            raise exceptions.AuthenticationFailed()
        '''
        # существует ли пользователь, чей id закодирован в токене?
        user = CustomUser.objects.get(pk = payload['id'])
        if user is None:
            msg = 'No user matching this token was found'
            raise exceptions.AuthenticationFailed(msg)

        return (user, token)
