from django.shortcuts import render
from rest_framework import viewsets
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import Dish
from .models import Order
from .serializers import DishSerializer
from .serializers import RegSerializer
from .serializers import LoginSerializer
from .serializers import OrderSerializer
from django.forms.models import model_to_dict
from rest_framework import generics
from datetime import datetime
from rest_framework.pagination import PageNumberPagination

# Create your views here.

class DishPagintation(PageNumberPagination):
    # localhost:8000/dish/?page=1 вернет первые 20
    page_size = 20
    page_query_param = 'page'
    max_page_size = 20


class DishList(generics.ListCreateAPIView):
    '''
    наследуемся от ListCreateAPIView, потому что его
    потомкам можно просто задать класс для настроек пагинации

    ListCreateAPIView поддерживает только GET, что нам и нужно
    '''
    permission_classes = [IsAuthenticated] # доступ по авторизации
    pagintation_class = DishPagintation
    queryset = Dish.objects.all().order_by('title')
    serializer_class = DishSerializer


class RegAPIView(APIView):
    '''
    Создаёт нового пользователя, возвращает токен
    '''
    permission_classes = [AllowAny] # доступ всем без авторизации

    def post(self, request):
        serializer = RegSerializer(data = request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()

        return Response(
            {
                'token': user.token
            },
            status=status.HTTP_201_CREATED,
        )


class LoginAPIView(APIView):
    '''
    Создаёт новый токен для сушествующего пользователя
    '''
    permission_classes = [AllowAny] # доступ всем без авторизации

    def post(self, request):
        serializers_class = LoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.get_user(serializer.validated_data)

        if user is None:
            return Response(
                status=status.HTTP_404_NOT_FOUND
            )

        return Response(
            {
                'token': user.token
            },
            status=status.HTTP_200_OK,
        )

class OrderAPIView(APIView):
    '''
    get/post user orders
    '''
    permission_classes = [IsAuthenticated]
    serializers_class = OrderSerializer

    def get(self, request):
        '''
        получить все order, связанные с текущем user
        '''
        orders = request.user.order_set.all().order_by('date')
        '''
        мы можем получить user через request.user, т.к
        установили permission_classes = [IsAuthenticated]
        (запросы доступны только для авторизованных пользователей)
        и jwt_auth в качестве аутентификатора - в котором возвращается
        пользователь по токену, содержащемуся в запросе
        '''
        serializer = OrderSerializer(orders, many = True)
        return Response(
            {
                'orders': serializer.data
            },
            status = status.HTTP_200_OK
        )

    def post(self, request):
        '''
        создать заказ мне проще было прямо во вьюхах,
        потому что сериализовать, валидировать нечего -
        нам не нужны никакие данные от пользователя чтобы
        создать order
        '''
        order = Order.objects.create(
            date = datetime.now(),
            order_status = 'N',
            customer = request.user,
            comment = ''
        )
        '''
        по тз в запросе будет массив айдишников заказанных блюд
        для каждого id создаём модель DishInOrder, все DishInOrder
        будут связаны с одним заказом, который мы передадим
        в serializer с помощью словаря context
        '''
        context = {"order": order}
        serializer = DishInOrderSerializer(request.data, many = True, context = context)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(
            {
                'order': order
            },
            status = status.HTTP_200_OK
        )
