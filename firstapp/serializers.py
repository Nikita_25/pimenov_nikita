from rest_framework import serializers
from .models import Dish
from .models import CustomUser
from .models import Order
from .models import DishInOrder
from .phone_auth import authenticate

class DishSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dish
        fields = ('type', 'cost')

class RegSerializer(serializers.ModelSerializer):
    '''
    Creates a new user
    '''
    first_name = serializers.CharField(max_length = 30)
    last_name = serializers.CharField(max_length = 30)
    phone = serializers.CharField(max_length = 12)
    password = serializers.CharField(max_length = 20)

    class Meta:
        model = CustomUser
        fields = ('first_name', 'last_name', 'phone', 'password')

    def create(self, validated_data):
        return CustomUser.objects.create_user(**validated_data)

class LoginSerializer(serializers.Serializer):
    '''
    Authenticates an existing user
    '''
    phone = serializers.CharField(max_length = 12)
    password = serializers.CharField(max_length = 20)

    def validate(self, data):
        '''
        validates user data
        --
        Any function with `ValidationError` on failure
        could be used as validator
        in django rest's as `is_valid` method
        '''
        phone = data.get('phone', None)
        password = data.get('password', None)

        if phone is None:
            raise serializers.ValidationError(
                'A Phone is requiered to log in'
            )

        if password is None:
            raise serializers.ValidationError(
                'A password is requiered to log in'
            )

    def get_user(self, validated_data):
        '''
        Только проверка, существует ли пользователь!
        нужно заменить authenticate на подходящее слово, но мне влом
        '''
        return authenticate(
            phone=validated_data['phone'],
            password=validated_data['password']
        )

class CustomUserSerializer(serializers.ModelSerializer):
    phone = serializers.CharField(max_length = 12)
    password = serializers.CharField(max_length = 30)
    first_name = serializers.CharField(max_length = 30)
    last_name = serializers.CharField(max_length = 30)


class OrderSerializer(serializers.ModelSerializer):

    class Meta:
        model = Order
        fields = ['date', 'customer', 'order_status', 'comment']
        depth = 1


class DishInOrderSerializer(serializers.ModelSerializer):
    order = serializers.StringRelatedField(read_only=True, source='order')
    dish = serializers.StringRelatedField(read_only=True, many = True, source='dish')

    class Meta:
        model = DishInOrder
        fields = ('order', 'dish')

    def validate_dish(self, data):
        dish_id = data.get('dish')
        if DishInOrder.objects.get(pk=dish_id) is None:
            raise ValidationError(
                'no such dish'
            )

    def create(self, validated_data):
        order = self.context.get('order')
        dish = validated_data['dish']

        return DishInOrder.objects.create(
            order=order,
            dish=dish
        )
