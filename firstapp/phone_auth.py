from .models import CustomUser

def authenticate(phone=None, password=None):
    user = CustomUser.objects.get(phone=phone)
    if user is not None:
        '''
        пароли нужно проверять именно так, а не ==, потому что они зашифрованы
        в базе и простое сравнение с вводом пользователя вернёт false
        '''
        if user.check_password(password) is True:
            return user
        else:
            return None
    return None
