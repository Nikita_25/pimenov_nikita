from django.contrib.auth.base_user import BaseUserManager

class CustomUserManager(BaseUserManager):
    user_in_migrations = True

    def create_user(self, phone, first_name, last_name, password):
        if not phone:
            raise ValueError("")
        user = self.model(
            phone = phone,
            first_name = first_name,
            last_name = last_name,
            is_staff = False,
            is_superuser = False,
        )
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, phone, password):
        if not phone:
            raise ValueError("The given phone must be set")
        user = self.model(
            phone = phone,
            is_staff = True,
            is_superuser = True,
        )
        user.set_password(password)
        user.save()
        return user
