from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.utils.translation import gettext_lazy as _
from .managers import CustomUserManager
import jwt
from datetime import datetime, timedelta
from hello import settings

# Create your models here.
class Dish(models.Model):
    title = models.CharField(max_length = 30)
    description = models.TextField()
    weight = models.PositiveSmallIntegerField()
    cost = models.FloatField()
    photo = models.URLField(blank = True)
    type = models.CharField(max_length = 30)

class CustomUser(AbstractBaseUser, PermissionsMixin):
    first_name = models.CharField(max_length = 30)
    last_name = models.CharField(max_length = 30)
    phone = models.CharField(_('phone'), max_length = 12, unique = True)
    password = models.CharField(max_length = 20)
    is_staff = models.BooleanField(default = False)
    is_superuser = models.BooleanField(default = False)
    is_active = models.BooleanField(default = True)

    USERNAME_FIELD = 'phone'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    @property
    def token(self):
        return self._generate_jwt_token()

    def _generate_jwt_token(self):
        dt = datetime.now() + timedelta(days=60)
        token = jwt.encode({
            'id': self.pk,
            'exp': int(dt.strftime('%s'))
        }, settings.SECRET_KEY, algorithm='HS256')

        return token.decode('utf-8')

    def get_full_name(self):
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        return self.first_name


class Order(models.Model):
    order_status_choices = [
        ('N', 'new'),
        ('P', 'processing'),
        ('W', 'in the way'),
        ('D', 'delivered'),
    ]
    order_status = models.CharField(
        max_length = 1,
        choices = order_status_choices,
        default = 'N',
    )
    date = models.DateField(default='N') #
    customer = models.ForeignKey(CustomUser, on_delete = models.CASCADE)
    comment = models.TextField(blank=True)

class DishInOrder(models.Model):
    order = models.ForeignKey(Order, on_delete = models.CASCADE)
    dish = models.ForeignKey(Dish, on_delete = models.DO_NOTHING)
